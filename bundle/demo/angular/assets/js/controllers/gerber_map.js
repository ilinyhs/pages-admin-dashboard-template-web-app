'use strict';

/* Controllers */

angular.module('app')
// Gerber Map controller
    .controller('GerberMapCtrl', ['$scope', function ($scope) {

        console.log('> GerberMapCtrl');//todo


        /**
         * gerber-layer.js
         */
        // We expect SVGs to be in mil (svgerber seems to do this), but I prefer mm
        var MM_PER_MIL = 0.0254;
        var DEG_PER_MM = 1/111194925;// = (360 deg) / (2 * PI * Re);  // Re = 6371000000 mm - radius of Earth in mm;
        var DEG_PER_MIL = MM_PER_MIL * DEG_PER_MM;
        var GerberLayer = L.Class.extend({
            initialize: function (index, map, info, loadCallback) {
                // create a div for the SVG; Leaflet wants this.
                this._el = L.DomUtil.create('div', 'gerber-layer');

                // start an XHR to load the SVG
                this._request = new XMLHttpRequest();
                this._request._parent = this;
                this._request.onreadystatechange = this.onLoaded;
                this._request.open('GET', info.svg);
                this._request.send();

                // rendering info
                this._color          = info.color;
                this._zIndex         = index;
                this._el.style.color = info.color;	// the SVGs are set up so that the CSS color attribute determines their render color

                this._fn             = info.svg;
                this._loaded         = false;
                this._loadCallback   = loadCallback;
            },

            onLoaded: function() {
                if (this.readyState === 4 && this.status === 200) {
                    // the SVG was loaded succcessfully, put it in the document
                    var xmldoc = this.responseXML,
                        svg    = xmldoc.getElementsByTagName('svg')[0];

                    var p = this._parent;

                    // pull viewBox info from the SVG, which reflect its position and size in PCB coordinates
                    var vx = svg.viewBox.baseVal.x      * DEG_PER_MIL,
                        vy = svg.viewBox.baseVal.y      * DEG_PER_MIL,
                        vw = svg.viewBox.baseVal.width  * DEG_PER_MIL,
                        vh = svg.viewBox.baseVal.height * DEG_PER_MIL;

                    // turn those into two lat/long pairs for determining the SVG's position so it aligns right
                    // remember, latitude is Y
                    var northWest  = new L.latLng(vy+vh, vx),
                        southEast  = new L.latLng(vy, vx+vw);

                    p._bounds = L.latLngBounds([northWest, southEast]);

                    // zoom animations
                    if (p._map.options.zoomAnimation && L.Browser.any3d) {
                        L.DomUtil.addClass(svg, 'leaflet-zoom-animated');
                    } else {
                        L.DomUtil.addClass(svg, 'leaflet-zoom-hide');
                    }

                    svg.style.zIndex = p._zIndex;
                    p._svg = svg;
                    p._el.appendChild(svg);

                    p._reset();

                    p._loaded = true;
                    p._loadCallback(this);
                }
            },

            getBounds: function() {
                // return the bounds
                return this._bounds;
            },

            onAdd: function (map) {
                this._map = map;

                // create a DOM element and put it into one of the map panes
                map.getPanes().overlayPane.appendChild(this._el);
                if(this._svg)
                    this._svg.style.zIndex = this._zIndex;

                map.on('viewreset', this._reset, this);

                if (map.options.zoomAnimation && L.Browser.any3d) {
                    map.on('zoomanim', this._animateZoom, this);
                }

                this._reset();
            },

            onRemove: function (map) {
                // remove layer's DOM elements and listeners
                map.getPanes().overlayPane.removeChild(this._el);
                map.off('viewreset', this._reset, this);

                if (map.options.zoomAnimation) {
                    map.off('zoomanim', this._animateZoom, this);
                }
            },

            _animateZoom: function (e) {
                if(this._svg) {
                    console.log(e);
                    var topLeft     = this._map._latLngToNewLayerPoint(this._bounds.getNorthWest(), e.zoom, e.center),
                        bottomRight = this._map._latLngToNewLayerPoint(this._bounds.getSouthEast(), e.zoom, e.center),
                        scale       = this._map.getZoomScale(e.zoom);

                    this._svg.style[L.DomUtil.TRANSFORM] = L.DomUtil.getTranslateString(topLeft) + ' scale(' + scale + ') ';
                }
            },

            _reset: function () {
                if(this._svg) {
                    var topLeft     = this._map.latLngToLayerPoint(this._bounds.getNorthWest()),
                        bottomRight = this._map.latLngToLayerPoint(this._bounds.getSouthEast());

                    // update the SVG position and size so that the layers align properly
                    this._svg.width.baseVal.value  = bottomRight.x - topLeft.x;
                    this._svg.height.baseVal.value = bottomRight.y - topLeft.y;
                    L.DomUtil.setPosition(this._svg, topLeft);
                }
            }
        });


        /**
         * notes-layer.js
         */
        (function () {

            var commentMarkers = {};
            var rulerMarkers = {};
            var lastRulerId = 0;
            var lastCommentId = 0;
            var isToolsOn = false;
            var startLatlng = null;
            var _map = null;
            var usedToolType = null;
            var commentsStorage;
            var rulerStorage;
            var popupCreator;
            var setPopupCreatorCommentId;

            // gerber viewer tools namespace
            L.GvNotesTools = {};

            L.GvNotesTools.NotesToolsControl = L.Control.extend({
                options: {
                    position: 'bottomleft'
                },
                onAdd: function (map) {
                    _map = map;

                    var control = L.DomUtil.create('div', 'leaflet-control');

                    var circleControl = L.DomUtil.create('div', 'map-tool comment', control);
                    circleControl.innerHTML = '<div>Comment</div>';

                    L.DomEvent.addListener(circleControl, 'click', function (e) {
                        usedToolType = 'circle';
                        onClickControl(e);
                    });

                    var rulerControl = L.DomUtil.create('div', 'map-tool measure', control);
                    rulerControl.innerHTML = '<div>Measure</div>';

                    L.DomEvent.addListener(rulerControl, 'click', function (e) {
                        usedToolType = 'ruler';
                        onClickControl(e);
                    });

                    return control;
                }
            });

            L.GvNotesTools.NotesLayer = L.Class.extend({

                initialize: function (map) {
                    _map = map;
                    commentsStorage = new MarkerStorage('comments.json');
                    rulerStorage = new MarkerStorage('lines.json');
                },
                onRemove: function (map) {
                },
                onAdd: function (map) {
                    this._initStorages();
                    this._initPopupCreator();
                },
                _initStorages: function () {
                    var overlays;

                    var maxCommentId = lastCommentId;
                    commentMarkers = commentsStorage.getAll();
                    commentMarkers.forEach(function (marker) {
                        if (marker._id > maxCommentId) {
                            maxCommentId = marker._id;
                        }
                        overlays = addComment(marker);
                        addCommentPopup(marker, overlays[0]);
                    });
                    lastCommentId = maxCommentId;

                    var maxRulerId = lastRulerId;
                    rulerMarkers = rulerStorage.getAll();
                    rulerMarkers.forEach(function (marker) {
                        if (marker._id > maxRulerId) {
                            maxRulerId = marker._id;
                        }
                        overlays = addRuler(marker);
                        addRulerPopup(marker, overlays[0]);
                    });
                    lastRulerId = maxRulerId;
                },
                _initPopupCreator: function () {
                    var content = L.DomUtil.create('div', 'popup-comment');
                    content.innerHTML =
                        '<div class="number-circle"><span id="markerId"></span></div>' +
                        '<select id="commentType">' +
                        '   <option value="Problem">Problem</option>' +
                        '   <option value="Suggestion">Suggestion</option>' +
                        '</select>' +
                        '<br/>' +
                        '<textarea placeholder="Write a description" autofocus maxlength="500"></textarea>' +
                        '<br/>' +
                        '<input type="button" value="Save">' +
                        '<input type="button" value="Cancel">'
                    ;

                    popupCreator = L.popup()
                        .setContent(content);

                    setPopupCreatorCommentId = function (markerIdContent) {
                        markerId.innerHTML = markerIdContent;
                    };

                    var markerId = content
                        .getElementsByClassName('number-circle')[0]
                        .getElementsByTagName('span')[0];
                    var select = content.getElementsByTagName('select')[0];
                    var textarea = content.getElementsByTagName('textarea')[0];
                    var inputs = content.getElementsByTagName('input');

                    L.DomEvent.addListener(inputs[0], 'click', function (e) {
                        markerId.innerHTML = (++lastCommentId);
                        var endLatlng = popupCreator.getLatLng();
                        var distanceMeters = endLatlng.distanceTo(startLatlng);// in meters
                        var commentMarker = {
                            _id: lastCommentId,
                            _centerLatlng: startLatlng,
                            _radius: distanceMeters * 1000,
                            title: select.value,
                            body: textarea.value,
                            author: 'jimb@jim.com',
                            date: _formatDate(new Date())
                        };
                        commentsStorage.saveOne(commentMarker);
                        var overlays = addComment(commentMarker);
                        addCommentPopup(commentMarker, overlays[0]);
                        // .openOn(_map);

                        popupCreator._close();
                        _clearPopupCreator();
                        startLatlng = null;
                    });
                    L.DomEvent.addListener(inputs[1], 'click', function (e) {
                        popupCreator._close();
                        _clearPopupCreator();
                        startLatlng = null;
                    });

                    function _clearPopupCreator() {
                        select.value = 'Problem';
                        textarea.value = '';
                        markerId.innerHTML = '';
                    }

                }
            });

            function onClickControl(e) {
                if (!isToolsOn) {
                    _map.dragging.disable();
                    isToolsOn = true;
                    setCursor();
                    _map.once('mousedown', onMousedown);
                } else {
                    _map.dragging.enable();
                    isToolsOn = false;
                    resetCursor();
                    _map.off('mousedown', onMousedown);
                }
            }

            function setCursor() {
                var leafletContainer = document.getElementsByClassName('leaflet-container');
                leafletContainer[0].classList.add('crosshair-cursor');

                var leafletClickable = document.getElementsByClassName('leaflet-clickable');
                for (var i = 0; i < leafletClickable.length; ++i) {
                    leafletClickable[i].classList.add('crosshair-cursor');
                }
            }

            function resetCursor() {
                var leafletContainer = document.getElementsByClassName('leaflet-container');
                leafletContainer[0].classList.remove('crosshair-cursor');

                var leafletClickable = document.getElementsByClassName('leaflet-clickable');
                for (var i = 0; i < leafletClickable.length; ++i) {
                    leafletClickable[i].classList.remove('crosshair-cursor');
                }
            }

            function onMousedown(e) {
                _map.once('mouseup', onMouseup);
                _map.on('mousemove', onMousemove);
                startLatlng = e.latlng;
            }

            function onMouseup(e) {
                if (!isToolsOn) {
                    return;
                }

                _removeDrawableElements();

                if (startLatlng) {
                    var endLatlng = e.latlng;

                    if (usedToolType === 'circle') {
                        setPopupCreatorCommentId(lastCommentId + 1);
                        popupCreator
                            .setLatLng(endLatlng)
                            .openOn(_map);
                    } else  /*if (usedToolType === 'ruler')*/ {
                        var distanceMeters = endLatlng.distanceTo(startLatlng);// in meters
                        var rulerMarker = {
                            _id: ++lastRulerId,
                            _startLatlng: startLatlng,
                            _endLatlng: endLatlng,
                            reason: 'reason',
                            distance: distanceMeters * 1000
                        };
                        rulerStorage.saveOne(rulerMarker);
                        var overlays = addRuler(rulerMarker);
                        addRulerPopup(rulerMarker, overlays[0])
                            .openOn(_map);
                        startLatlng = null;
                    }
                }

                _map.off('mousemove', onMousemove);
                _map.dragging.enable();
                isToolsOn = false;
                resetCursor();
            }

            var drawableElements = [];

            function _removeDrawableElements() {
                if (drawableElements.length > 0) {
                    drawableElements.forEach(function (drawableElement) {
                        _map.removeLayer(drawableElement);
                    });
                }
            }

            function onMousemove(e) {
                _removeDrawableElements();

                var endLatlng = e.latlng;
                var distanceMeters = endLatlng.distanceTo(startLatlng);// in meters

                if (usedToolType === 'circle') {
                    var commentMarker = {
                        _id: null,
                        _centerLatlng: startLatlng,
                        _radius: distanceMeters * 1000
                    };
                    drawableElements = addComment(commentMarker);
                } else  /*if (usedToolType === 'ruler')*/ {
                    var rulerMarker = {
                        _id: null,
                        _startLatlng: startLatlng,
                        _endLatlng: endLatlng
                        // ,distance: distanceMeters * 1000
                    };
                    drawableElements = addRuler(rulerMarker);
                }
            }

            function addComment(marker) {
                var circle = L.circle(marker._centerLatlng, marker._radius / 1000, {
                    color: "orange",
                    fillOpacity: 0.0,
                    opacity: 1
                });
                circle.addTo(_map);
                return [circle];
            }

            function addRuler(marker) {
                var tailOptions = {
                    opacity: 1,
                    color: 'black',
                    weight: 1,
                    fillColor: 'black',
                    fillOpacity: 1
                };
                var radius = 3;

                var startTail = L.circleMarker(marker._startLatlng, tailOptions)
                    .setRadius(radius)
                    .addTo(_map);

                var endTail = L.circleMarker(marker._endLatlng, tailOptions)
                    .setRadius(radius)
                    .addTo(_map);

                var line = L.polyline([marker._startLatlng, marker._endLatlng], {
                    opacity: 1,
                    color: 'black',
                    weight: 3
                });
                line.addTo(_map);
                return [line, startTail, endTail];
            }

            function addCommentPopup(marker, overlay/*Path class*/) {
                var content = L.DomUtil.create('div', 'popup-comment');
                content.innerHTML =
                    '<div class="comment-header">' +
                    '   <div class="number-circle"><span>' + marker._id + '</span></div>' +
                    '   <h4>' + marker.title + '</h4>' +
                    '</div>' +
                    '<p class="comment-description"><b>' + marker.body + '</b></p>' +
                    '<div class="comment-info"><b>Author:</b> ' + marker.author + '</div>' +
                    '<div class="comment-info"><b>Date:</b> ' + marker.date + '</div>';

                var popup = L.popup()
                    .setLatLng(marker._centerLatlng)
                    .setContent(content);

                overlay.bindPopup(popup);

                return popup;
            }

            function addRulerPopup(marker, overlay/*Path class*/) {
                var content = L.DomUtil.create('div', 'popup-comment');
                content.innerHTML = '<b>Distance:</b>' + marker.distance.toFixed(2) + ' mm';

                var middle = L.latLng(
                    (marker._endLatlng.lat + marker._startLatlng.lat) / 2,
                    (marker._endLatlng.lng + marker._startLatlng.lng) / 2
                );

                var popup = L.popup()
                    .setLatLng(middle)
                    .setContent(content);

                overlay.bindPopup(popup);

                return popup;
            }

            function MarkerStorage(storageName) {
                var _this = this;
                this._storageName = storageName;
                this._markers = {};// map: markerId -> marker
                _getMarkers();

                this.getAll = _getMarkers;
                this.saveOne = _saveMarker;
                this.removeOne = _removeMarker;

                function _getMarkers() {
                    return JSON.parse(localStorage.getItem(_this._storageName)) || [];
                }

                function _saveMarker(marker) {
                    _this._markers = _getMarkers();
                    var foundPos = _findPosition(marker._id);
                    if (foundPos === null) {
                        _this._markers.push(marker);
                    } else {
                        _this._markers[foundPos] = marker;
                    }
                    localStorage.setItem(_this._storageName, JSON.stringify(_this._markers));
                }

                function _removeMarker(markerId) {
                    _this._markers = _getMarkers();
                    var foundPos = _findPosition(markerId);
                    if (foundPos !== null) {
                        _this._markers.splice(foundPos, 1);
                        localStorage.setItem(_this._storageName, JSON.stringify(_this._markers));
                    }
                }

                function _findPosition(markerId) {
                    var foundPos = null;
                    _this._markers.some(function (marker, pos) {
                        var match = marker._id === markerId;
                        if (match) {
                            foundPos = pos;
                        }
                        return match;
                    });
                    return foundPos;
                }
            }

            function _formatDate(date) {
                var d = date.getDate();
                var y = date.getFullYear();
                var m = date.getMonth();
                return (m + 1).toString() + '/' + d + '/' + y;
            }
        })();


        /**
         * gerber-viewer.js
         */
        var SCALE = 1.;	// The scale we render things at. Units are whatever Leaflet uses per millimetre.

// The CRS we use for displaying maps. It's like the Simple CRS but has a scale factor.
        L.CRS.SimpleScaled = L.extend({}, L.CRS, {
            projection: L.Projection.LonLat,
            transformation: new L.Transformation(SCALE, 0, -SCALE, 0),

            scale: function (zoom) {
                return Math.pow(1.5, zoom);
            }
        });

// warning and error markers
        L.Icon.Warning = L.icon({
            iconUrl: 'pages/img/icons/gerber-map/warning.png?123',
            iconSize: [41, 36],
            popupAnchor: [0, -16]
        });

// warning and error markers
        L.Icon.Error = L.icon({
            iconUrl: 'pages/img/icons/gerber-map/error.png',
            iconSize: [41, 36],
            popupAnchor: [0, -16]
        });

        function boardInfoLoaded() {
            // called when the board info XHR updates status
            if(this.readyState != 4)
                return;

            if(this.status != 200)
                return;

            // at this point we can assume the board info was loaded.
            // console.log(info);
            var info = JSON.parse(this.responseText);
            // console.log(info);
            var layerObjects = {}, map = this.map;
            var layerLoaded = function(l) {
                // called when the layer SVG has been loaded.

                // check whether all layers have loaded
                for(var i in layerObjects) {
                    if(!layerObjects[i]._loaded)
                        return;
                }

                // if so, find the total bounds of the board and zoom to it
                var boardBounds = L.latLngBounds([]);
                for(var i in layerObjects) {
                    boardBounds.extend(layerObjects[i].getBounds());
                }

                map.fitBounds(boardBounds);
            };

            // add layers
            for(var i=0; i<info.layers.length; i++) {
                var layer = info.layers[i];
                // var l = new GerberLayer(i, this.map, layer, layerLoaded);
                var l = new GerberLayer(0, this.map, layer, layerLoaded);// todo used zindex=0 for all svg
                layerObjects[layer.name] = l;
                this.map.addLayer(l);
            }

            L.control.layers([], layerObjects).addTo(this.map);

            this.map.addLayer(L.GvNotesTools.notesLayer = new L.GvNotesTools.NotesLayer(this.map));

            for(var i=0; i<info.notes.length; i++) {
                var note = info.notes[i];

                var item = L.DomUtil.create("li", "note " + note.type, this.notes);

                if(!note.pos) {
                    item.innerHTML = "<b>global " + note.type + ":</b><br>" + note.message;
                } else {
                    item.innerHTML = "<b>" + note.type + " at (" + note.pos[0] + "," + note.pos[1] +"):</b><br>" + note.message;

                    // add a marker
                    var markerPos = L.latLng(note.pos[1]*DEG_PER_MIL, note.pos[0]*DEG_PER_MIL); // latLng are effectively (y,x) so reverse

                    var marker;
                    switch(note.type) {
                        case "warning":
                            marker = L.marker(markerPos, {icon: L.Icon.Warning}).addTo(this.map);
                            marker.bindPopup("<b>warning:</b> " + note.message);
                            break;

                        case "error":
                            marker = L.marker(markerPos, {icon: L.Icon.Error}).addTo(this.map);
                            marker.bindPopup("<b>error:</b> " + note.message);
                            break;
                    }

                    item.marker = marker;
                    item.addEventListener("click", function() { this.marker.openPopup(); });
                }
            }
        }

        function loadViewers() {
            var viewers = document.getElementsByClassName("board-viewer");
            Array.prototype.forEach.call(viewers, function(viewer) {
                // request board data file
                var request = new XMLHttpRequest();
                request.addEventListener("readystatechange", boardInfoLoaded);
                request.open('GET', viewer.dataset.boardUrl);

                // add map in preparation
                var div   = viewer.getElementsByClassName("map")[0];
                var notes = viewer.getElementsByClassName("notes")[0];
                var map = L.map(div, {crs: L.CRS.SimpleScaled}).setView(L.latLng(50*DEG_PER_MIL,50*DEG_PER_MIL), 0);
                map.addControl(new L.GvNotesTools.NotesToolsControl());

                request.viewer = viewer;
                request.map    = map;
                request.notes  = notes;

                request.send();
            } );
        }

        loadViewers();// document.addEventListener("DOMContentLoaded", loadViewers);


    }]);
